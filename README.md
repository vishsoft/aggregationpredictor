# aggregationpredictor

This R package offers a browser-based interface for determining the type of aggregation that is likely (H/J aggregation) given the dye structure. 

To use the package you will need to install R (https://cran.r-project.org/bin/windows/base/) and Java (https://www.java.com/en/download/)


## Package installation
* Download the tar.gz file and save it in a convenient place such as your desktop.
* Go into R, click on Packages (at the top of the R console), then click on "Install package(s) from local zip files", then select the file where you just saved it.
* You can also run the following command on the R console as > install.packages(path_to_file, repos = NULL, type="source") 

## Usage
Having installed the package, type the following on the R console

```
> library(aggregationpredictor)

> launchApp()
```
